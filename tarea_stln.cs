﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace TAREA1
{
  
    class Program
    {

        static void Main(string[] args)
        {
            Console.Title = "Estructura de Datos";
            Console.WriteLine("Ingrese el numero dependiendo de lo que desea ver");
            int num;
            num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case 1://listas
                    List<string> Lista = new List<string>();
                    //insercion 
                    Console.WriteLine("/////////////////////////////////////////////////");
                    Lista.Insert(0, "Datos Importantes de la tienda la Sirena");
                    Lista.Insert(1, "Datos Importantes de la tienda Jumbo");
                    Lista.Insert(2, "Datos Importantes de la tienda Plaza Lama");
                    Lista.Insert(2, "Datos Importantes de la tienda Plaza la Maxima");
                    //eliminacion                
                    Lista.Remove("Datos Importantes de la tienda Jumbo");
                    //eliminacion con removeat
                    Lista.RemoveAt(2);
                    //mostrar datos
                    foreach (string name in Lista)
                    {

                        Console.WriteLine(name);
                    }
                    //buscar
                    Console.WriteLine(Lista.Contains("Datos Importantes de la tienda la Sirena"));
                    Console.WriteLine(Lista.Contains("Datos Importantes de la tienda Jumbo"));

                    break;
                case 2://pilas

                    Stack<string> pila = new Stack<string>();

                    pila.Push("a");
                    pila.Push("b");
                    pila.Push("c");
                    pila.Push("d");
                    pila.Push("e");

                    pila.Pop();//eliminar ultimo agregado

                    Console.WriteLine("Primer elemento: " + pila.Peek());//muestra el primer elemento

                    Console.WriteLine("Num elementos: " + pila.Count);//muestra cantidade elementos

                    foreach (string p in pila)//mostrar pila
                    {
                        Console.WriteLine(p);
                    }

                    break;

                case 3:// colas
                    Queue cola = new Queue();
                    cola.Enqueue("Hola,");
                    cola.Enqueue("soy");
                    cola.Enqueue("yo");
                    foreach(string c in cola)
                    {
                        Console.WriteLine(c);
                    }
                    break;
                case 4://grafos
                    Console.WriteLine("datos4");
                    break;
                case 5://arboles n-arios
                    Console.WriteLine("datos5");
                    break;
            }
        }
    }
}
